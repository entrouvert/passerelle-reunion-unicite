# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError
import psycopg2

class UnicityReunionConnector(BaseResource):

    field_name_1 = models.CharField(blank=False, default='f_siren', max_length=128, verbose_name=_(u'Nom du 1er champ à contrôler'))
    field_name_2 = models.CharField(blank=True, max_length=128, verbose_name=_(u'Nom du 2ème champ à contrôler (optionnel)'))
    db_name = models.CharField(blank=False, default='wcs_instance', max_length=128, verbose_name=_(u'Nom de la base'))
    db_user = models.CharField(blank=False, default='passerelle', max_length=128, verbose_name=_(u'Utilisateur de la base'))
    view_name = models.CharField(blank=False, default='wcs_view_formid_formname', max_length=128, verbose_name=_(u'Nom de la vue'))

    category = 'Divers'

    class Meta:
        verbose_name = u'Connecteur unicité Région Réunion'

    @endpoint(description=_(u'Vérifier s\'il y a déjà une demande dans la vue avec les valeurs des champs fournies en paramètre'),
          perm='can_access',
          parameters={
              'value_1': {'description': _(u'Champ 1'),
                        'example_value': u'77567227216096'},
              'value_2': {'description': _(u'Champ 2'),
                        'example_value': u'2020'},
              'form_id': {'description': _(u'Id de la demande'),
                        'example_value': u'1'}
              }
          )
    def unicite(self, request, value_1, value_2="", form_id=0):
        try:
            connection = psycopg2.connect(
                user = self.db_user,
                database = self.db_name
            )

            cursor = connection.cursor()
            if not value_2 or not self.field_name_2:
                cursor.execute("SELECT " + self.field_name_1 + " FROM " + self.view_name + " WHERE status != 'draft' AND id != %s AND " + self.field_name_1 + "=%s", (form_id, value_1))
            else:
                cursor.execute("SELECT " + self.field_name_1 + " FROM " + self.view_name + " WHERE status != 'draft' AND id != %s AND " + self.field_name_1 + "=%s AND " + self.field_name_2 + "=%s", (form_id, value_1, value_2))
            record = cursor.fetchone()
        except psycopg2.Error as e:
            raise APIError(e)
        finally:
            if(connection):
                cursor.close()
                connection.close()

        return {'data': record}

