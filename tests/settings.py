import os


INSTALLED_APPS += ('passerelle_reunion_unicite',)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'TEST': {
            'NAME': 'passerelle-reunion-unicite-test-%s' % os.environ.get("BRANCH_NAME", "").replace('/', '-')[:63],
        }
    }
}
